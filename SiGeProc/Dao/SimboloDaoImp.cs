﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc;



namespace SiGeProc.Dao
{
    public class SimboloDaoImp:SimboloDao
    {
        SqlConnection con;

        public SimboloDaoImp()
        {
            this.con = new Coneccion("Comun_Sistemas").coneccion();
                

        }
        public List<Simbolo> PorModelo(int idModelo)
        {
          
            List<Simbolo> simbolos = new List<Simbolo>();
            Simbolo simbolo = null;
          

        
            try
            {
                using (con)
                {
                  

                    string sConsulta = "Select s.Id_Simbolo, s.Id_Modelo,s.Tipo_Simbolo,s.Nombre_Simbolo,isnull(s.Rut_Asignado,0) as Rut_Asignado," +
                        " isnull(s.Rol_Asignado,0) as Rol_Asignado,isnull(s.Formulario,'') as Formulario " +
                        " From Simbolo as s Where s.Id_Modelo = @Id_Modelo";

                    con.Open();

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;

                    query.Parameters.Add(new SqlParameter("@id_modelo", idModelo));
                  
                    
                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            simbolo = new Simbolo(dr);

                            simbolos.Add(simbolo);

                        }

                       

                    
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }


          

            return simbolos;
        }

        public Simbolo PorId(int idSimbolo)
        {

           
            Simbolo s = null;



            try
            {
                using (con)
                {


                    string sConsulta = "Select s.Id_Simbolo, s.Id_Modelo,s.Tipo_Simbolo,s.Nombre_Simbolo,isnull(s.Rut_Asignado,0) as Rut_Asignado, " + 
                        " isnull(s.Rol_Asignado,0) as Rol_Asignado,isnull(s.Formulario,'') as Formulario  " +
                        " From Simbolo as s Where s.Id_Simbolo = @Id_Simbolo";

                    con.Open();

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;

                    query.Parameters.Add(new SqlParameter("@id_simbolo", idSimbolo));


                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                           s = new Simbolo(dr);

                         

                        }




                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }




            return s;
        }

        public int Agregar(Simbolo s)
        {
            int idSimbolo = 0;

            try
            {
                using (con)
                {
                    con.Open();


                    string sConsulta = "Insert Into Simbolo(Id_Modelo,Nombre_Simbolo,Tipo_Simbolo,Rut_Asignado,Rol_Asignado,Formulario) " +
                   "Values(@Id_Modelo,@Nombre_Simbolo,@Tipo_Simbolo,@Rut_Asignado,@Rol_Asignado,@Formulario) " +
                    "select @Id_Simbolo = @@IDENTITY";
                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;


                
                    query.Parameters.Add(new SqlParameter("@Id_Modelo", s.Id_Modelo));
                    query.Parameters.Add(new SqlParameter("@Nombre_Simbolo", s.Nombre_Simbolo));
                    query.Parameters.Add(new SqlParameter("@Tipo_Simbolo", s.Tipo_Simbolo));
                    query.Parameters.Add(new SqlParameter("@Rut_Asignado", s.Rut_Asignado));
                    query.Parameters.Add(new SqlParameter("@Rol_Asignado", s.Rol_Asignado));
                    query.Parameters.Add(new SqlParameter("@Formulario", s.Formulario));
                    query.Parameters.Add(new SqlParameter("@Id_Simbolo", s.Id_Simbolo)).Direction = ParameterDirection.Output;


                    query.ExecuteNonQuery();

                    idSimbolo= Convert.ToInt32(query.Parameters["@Id_Simbolo"].Value);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return idSimbolo;

        }

        public void Eliminar(int idSimbolo, int idModelo)
        {

            try
            {
                using (con)
                {
                    con.Open();
                    string sConsulta = "Delete Simbolo " +
                     " Where Id_Simbolo = @Id_Simbolo and Id_Modelo=@Id_Modelo ";

                    var query = new SqlCommand(sConsulta, con);

                    query.Parameters.Add(new SqlParameter("@Id_Simbolo", idSimbolo));
                    query.Parameters.Add(new SqlParameter("@Id_Modelo", idModelo));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }


        }




    }
}
