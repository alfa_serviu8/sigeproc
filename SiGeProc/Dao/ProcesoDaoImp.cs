﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc;



namespace SiGeProc.Dao
{
    public class ProcesoDaoImp:ProcesoDao
    {
        SqlConnection con;

        public ProcesoDaoImp()
        {
            this.con = new Coneccion("comun_sistemas").coneccion();
                

        }
        public PaginaProceso Listado(ProcesoBuscado pBuscado)
        {
          
            List<Proceso> procesos = new List<Proceso>();
            Proceso proceso = null;
            int totalRegistros=0;

        
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("proceso_listado_paginado", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@vigente", pBuscado.proceso.Vigente));
                  
                    query.Parameters.Add(new SqlParameter("@pagina_Solicitada", pBuscado.pagina.Numero()));
                    query.Parameters.Add(new SqlParameter("@tamacno_Pagina", pBuscado.pagina.Tamacno()));
                   
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;


                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            proceso = new Proceso(dr);

                            procesos.Add(proceso);

                        }

                       

                        totalRegistros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }


            PaginaProceso pp = new PaginaProceso(procesos, totalRegistros);

            return pp;
        }

        public Proceso PorSistema(int idSistema)
        {

            
            Proceso pr = null;



            try
            {
                using (con)
                {


                    string sConsulta = "Select p.Id_Proceso " +
                        " From Proceso as p Where p.Id_Sistema = @Id_Sistema";

                    con.Open();

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;

                    query.Parameters.Add(new SqlParameter("@id_sistema", idSistema));


                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            pr = new Proceso(dr);

                            

                        }




                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }




            return pr;
        }


        public void Agregar(Proceso p)
        {


            try
            {
                using (con)
                {
                    con.Open();


                    string sConsulta = "Insert Into Proceso(Id_Proceso,Codigo_Proceso,Nombre_Proceso,Vigente,Numero_Resolucion,Fecha_Resolucion,EsInterno,Id_Sistema) " +
                   "Values(@Id_Proceso,@Codigo_Proceso,@Nombre_Proceso,@Vigente,@Numero_Resolucion,@Fecha_Resolucion,@EsInterno,@Id_Sistema) ";

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;


                    query.Parameters.Add(new SqlParameter("@Id_Proceso", p.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@Codigo_Proceso", p.Codigo_Proceso));
                    query.Parameters.Add(new SqlParameter("@Nombre_Proceso", p.Nombre_Proceso));
                    query.Parameters.Add(new SqlParameter("@Vigente", p.Vigente));
                    query.Parameters.Add(new SqlParameter("@Numero_Resolucion", p.Numero_Resolucion));
                    query.Parameters.Add(new SqlParameter("@Fecha_Resolucion", p.Fecha_Resolucion));
                    query.Parameters.Add(new SqlParameter("@Id_Sistema", p.Id_Sistema));
                    query.Parameters.Add(new SqlParameter("@EsInterno", p.EsInterno));



                    query.ExecuteNonQuery();


                }
            }
            catch (Exception ex)
            {
                throw;
            }


        }


        public void Eliminar(int idProceso)
        {

            try
            {
                using (con)
                {
                    con.Open();
                    string sConsulta = "Delete Proceso " +
                     " Where Id_Proceso = @Id_Proceso ";

                    var query = new SqlCommand(sConsulta, con);

                    query.Parameters.Add(new SqlParameter("@Id_Proceso", idProceso));
                   
                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }


        }





    }
}
