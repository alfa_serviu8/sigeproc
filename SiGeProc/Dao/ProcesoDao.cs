﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeProc;
using SiGeProc.Entidades;

namespace SiGeProc.Dao
{
    public interface ProcesoDao
    {
        PaginaProceso Listado(ProcesoBuscado pBuscado);

        Proceso PorSistema(int idSistema);

        void Agregar(Proceso p);

        void Eliminar(int idProceso);



    }
}
