﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc;



namespace SiGeProc.Dao
{
    public class FlujoDaoImp:FlujoDao
    {
        SqlConnection con;

        public FlujoDaoImp()
        {
            this.con = new Coneccion("Comun_Sistemas").coneccion();
                

        }

        public List<Flujo> PorModelo(int idModelo)
        {

            List<Flujo> listado = new List<Flujo>();
            Flujo f = null;



            try
            {
                using (con)
                {


                    string sConsulta = "Select f.Id_Simbolo,f.Id_Modelo, f.Id_Simbolo_Siguiente,isnull(f.Condicion,-1) as Condicion " +
                        
                        " From Flujo as f Where f.Id_Modelo = @Id_Modelo";

                    con.Open();

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;

                    query.Parameters.Add(new SqlParameter("@id_modelo", idModelo));


                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            f = new Flujo(dr);

                            listado.Add(f);

                        }




                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }




            return listado;
        }

        public List<Flujo> Siguientes(int idSimboloActual)
        {
          
            List<Flujo> siguientes = new List<Flujo>();
            Flujo siguiente = null;
          

        
            try
            {
                using (con)
                {
                  

                    string sConsulta = "Select f.Id_Simbolo,f.Id_Modelo, f.Id_Simbolo_Siguiente,isnull(f.Condicion,-1) as Condicion " +
                        " From Flujo as f Where f.Id_Simbolo = @Id_Simbolo";

                    con.Open();

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;

                    query.Parameters.Add(new SqlParameter("@id_simbolo", idSimboloActual));
                  
                    
                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            siguiente = new Flujo(dr);

                            siguientes.Add(siguiente);

                        }

                       

                    
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }


          

            return siguientes;
        }

        public List<Flujo> Anteriores(int idSimbolo)
        {

            List<Flujo> anteriores = new List<Flujo>();
            Flujo anterior = null;



            try
            {
                using (con)
                {


                    string sConsulta = "Select f.Id_Simbolo, f.Id_Simbolo_Siguiente,isnull(f.Condicion,-1) as Condicion " +
                        " From Flujo as f Where f.Id_Simbolo_Siguiente = @Id_Simbolo";

                    con.Open();

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;

                    query.Parameters.Add(new SqlParameter("@id_simbolo_siguiente", idSimbolo));


                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            anterior = new Flujo(dr);

                            anteriores.Add(anterior);

                        }




                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }




            return anteriores;        
        }


        public void Agregar(Flujo f)
        {

        
            try
            {
                using (con)
                {
                    con.Open();


                    string sConsulta = "Insert Into Flujo(Id_Simbolo,Id_Simbolo_Siguiente,Id_Modelo,Condicion) " +
                   "Values(@Id_Simbolo,@Id_Simbolo_Siguiente,@Id_Modulo,@Condicion) ";

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;


                    query.Parameters.Add(new SqlParameter("@Id_Simbolo", f.Id_Simbolo));
                    query.Parameters.Add(new SqlParameter("@Id_Simbolo_Siguiente", f.Id_Simbolo_Siguiente));
                    query.Parameters.Add(new SqlParameter("@Id_Modelo", f.Id_Modelo));
                    query.Parameters.Add(new SqlParameter("@Condicion", f.Condicion));
                          

                    query.ExecuteNonQuery();


                }
            }
            catch (Exception ex)
            {
                throw;
            }

        
        }

        public void Eliminar(int idSimbolo,int idSimbolo_Siguiente,int idModelo)
        {

            try
            {
                using (con)
                {
                    con.Open();
                    string sConsulta = "Delete Flujo " +
                     " Where Id_Simbolo = @Id_Simbolo and Id_Simbolo_Siguiente=@Id_Simbolo_Siguiente and Id_Modelo=@Id_Modelo ";

                    var query = new SqlCommand(sConsulta, con);

                    query.Parameters.Add(new SqlParameter("@Id_Simbolo", idSimbolo));
                    query.Parameters.Add(new SqlParameter("@Id_Simbolo_Siguiente", idSimbolo_Siguiente));
                    query.Parameters.Add(new SqlParameter("@Id_Modelo", idModelo));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }


        }



    }
}
