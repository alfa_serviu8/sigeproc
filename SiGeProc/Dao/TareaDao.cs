﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeProc;
using SiGeProc.Entidades;

namespace SiGeProc.Dao
{
    public interface TareaDao
    {
        PaginaTarea Listado(TareaBuscada tb);
        int Agregar(Tarea t);
        void Actualizar(Tarea t);

        void Completar(Tarea t);
      
    }
}
