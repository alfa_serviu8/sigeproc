﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc.Dao;



namespace SiGeProc.Dao
{
    public class InstanciaProcesoDaoImp:InstanciaProcesoDao
    {
        SqlConnection con;

        public InstanciaProcesoDaoImp()
        {
            this.con = new Coneccion("Comun_Sistemas").coneccion();

        }





        public int Agregar(InstanciaProceso ip)
        {

            int id_instancia = 0;
            string sConsulta = "Insert Into Instancia_Proceso(Id_Proceso,Fecha_Inicio,Estado) " +
           "Values(@Id_Proceso,getDate(),'Activa') " +

            "select @Id_Instancia_proceso = @@IDENTITY";

            var query = new SqlCommand(sConsulta, con);

            query.CommandType = CommandType.Text;

            query.Parameters.Add(new SqlParameter("@Id_Proceso", ip.Id_Proceso));
                         
            query.Parameters.Add(new SqlParameter("@Id_Instancia_Proceso", ip.Id_Instancia_Proceso)).Direction = ParameterDirection.Output;

            try
            {
                using (con)
                {
                    con.Open();

                    query.ExecuteNonQuery();

                    id_instancia = Convert.ToInt32(query.Parameters["@Id_Instancia_Proceso"].Value);
                }
            }

            catch (Exception ex)
            {
                throw;
            }
            return id_instancia;
        }



        public void Actualizar(InstanciaProceso ip)
        {


            string sConsulta = "Update Instancia_Proceso set estado=@estado  " +
           " where id_instancia_proceso = @id_instancia_proceso ";

            

            var query = new SqlCommand(sConsulta, con);

            query.CommandType = CommandType.Text;

            query.Parameters.Add(new SqlParameter("@Id_Instancia_Proceso", ip.Id_Proceso));

            query.Parameters.Add(new SqlParameter("@Estado", ip.Estado));

           
            try
            {
                using (con)
                {
                    con.Open();

                    query.ExecuteNonQuery();

                   
                }
            }

            catch (Exception ex)
            {
                throw;
            }
            
        }

        public void Completar(int id)
        {


            string sConsulta = "Update Instancia_Proceso set fecha_termino=getdate(),estado='Completada'  " +
           " where id_instancia_proceso = @id_instancia_proceso ";



            var query = new SqlCommand(sConsulta, con);

            query.CommandType = CommandType.Text;

            query.Parameters.Add(new SqlParameter("@Id_Instancia_Proceso", id));

            try
            {
                using (con)
                {
                    con.Open();

                    query.ExecuteNonQuery();


                }
            }

            catch (Exception ex)
            {
                throw;
            }

        }





    }




}
