﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc;



namespace SiGeProc.Dao
{
    public class ModeloDaoImp:ModeloDao
    {
        SqlConnection con;

        public ModeloDaoImp()
        {
            this.con = new Coneccion("Comun_Sistemas").coneccion();
                

        }
        public List<Modelo> PorProceso(int idProceso)
        {
          
            List<Modelo> modelos = new List<Modelo>();
            Modelo modelo = null;
          

        
            try
            {
                using (con)
                {
                  

                    string sConsulta = "Select m.Id_Modelo,m.Id_Proceso,m.Version From Modelo as m  " +
                   "Where m.Id_Proceso = @Id_Proceso  Order by m.Version";

                    con.Open();

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;

                    query.Parameters.Add(new SqlParameter("@id_proceso", idProceso));
                  
                    
                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            modelo = new Modelo(dr);

                            modelos.Add(modelo);

                        }

                       

                    
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
      

            return modelos;
        }

        public void Agregar(Modelo m)
        {


            try
            {
                using (con)
                {
                    con.Open();


                    string sConsulta = "Insert Into Modelo(Id_Modelo,Id_Proceso,Version) " +
                   "Values(@Id_Modelo,@Id_Proceso,@Version) ";

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;


                    query.Parameters.Add(new SqlParameter("@Id_Modelo", m.Id_Modelo));
                    query.Parameters.Add(new SqlParameter("@Id_Proceso", m.Id_Proceso));
                    query.Parameters.Add(new SqlParameter("@Version", m.Version));
                    


                    query.ExecuteNonQuery();


                }
            }
            catch (Exception ex)
            {
                throw;
            }


        }

        public void Eliminar(int idModelo)
        {

            try
            {
                using (con)
                {
                    con.Open();
                    string sConsulta = "Delete Modelo " +
                     " Where Id_Modelo = @Id_Modelo ";

                    var query = new SqlCommand(sConsulta, con);

                    query.Parameters.Add(new SqlParameter("@Id_Modelo", idModelo));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }


        }



    }
}
