﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc.Dao;



namespace SiGeProc.Dao
{
    public class TareaDaoImp:TareaDao
    {
        SqlConnection con;

        public TareaDaoImp()
        {
            this.con = new Coneccion("comun_sistemas").coneccion();

        }
        public PaginaTarea Listado(TareaBuscada tareaBuscada)
        {
          
            List<Tarea> tareas = new List<Tarea>();
            Tarea tarea = null;
            int totalRegistros=0;

           try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("tarea_listado_paginado", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@id_tarea", tareaBuscada.tarea.Id_Tarea));
                    query.Parameters.Add(new SqlParameter("@id_tipo_tarea", tareaBuscada.tarea.Id_Tipo_Tarea));
                    query.Parameters.Add(new SqlParameter("@rut_asignado", tareaBuscada.tarea.Rut_Asignado));
                    query.Parameters.Add(new SqlParameter("@rol_asignado", tareaBuscada.tarea.Rol_Asignado));
                    query.Parameters.Add(new SqlParameter("@vigente", tareaBuscada.tarea.Vigente));
                    query.Parameters.Add(new SqlParameter("@id_instancia_proceso", tareaBuscada.tarea.Id_Instancia_Proceso));
                    query.Parameters.Add(new SqlParameter("@pagina_Solicitada", tareaBuscada.pagina.Numero()));
                    query.Parameters.Add(new SqlParameter("@tamacno_Pagina", tareaBuscada.pagina.Tamacno()));
                   
                    query.Parameters.Add("@total_Registros", SqlDbType.Int).Direction = ParameterDirection.Output;


                    using (var dr = query.ExecuteReader())
                    {

                        while (dr.Read())
                        {
                            // Usuario
                            tarea = new Tarea(dr);

                            tareas.Add(tarea);

                        }

                       

                        totalRegistros = Convert.ToInt32(query.Parameters["@total_Registros"].Value);
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }


            PaginaTarea pt = new PaginaTarea(tareas, totalRegistros);

            return pt;
        }

        public int Agregar(Tarea t)
        {
            
            int id_tarea = 0;
               try
                {
                    using (con)
                    {
                        con.Open();

                  
                    string sConsulta = "Insert Into Tarea(Id_Tipo_Tarea,Rut_Asignado,Rol_Asignado,Fecha_Inicio,Vigente,Observaciones,Formulario,Id_Instancia_Proceso) " +
                   "Values(@Id_Tipo_Tarea,@Rut_Asignado,@Rol_Asignado,getDate(),1,'',@Formulario,@id_instancia_proceso) " +
                    "select @Id_Tarea = @@IDENTITY"; 

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;

                   
                   query.Parameters.Add(new SqlParameter("@Id_Tipo_Tarea", t.Id_Tipo_Tarea));
                       
                    query.Parameters.Add(new SqlParameter("@Rut_Asignado", t.Rut_Asignado));
                    query.Parameters.Add(new SqlParameter("@Rol_Asignado", t.Rol_Asignado));
                    query.Parameters.Add(new SqlParameter("@Formulario", t.Formulario));
                    query.Parameters.Add(new SqlParameter("@Id_Instancia_Proceso", t.Id_Instancia_Proceso));
            

                    query.Parameters.Add(new SqlParameter("@Id_Tarea", t.Id_Tarea)).Direction = ParameterDirection.Output;

                        query.ExecuteNonQuery();

                        id_tarea = Convert.ToInt32(query.Parameters["@Id_tarea"].Value);

                }
            }
                catch (Exception ex)
                {
                    throw;
                }

            return id_tarea;
            }


    

        public void Actualizar(Tarea t)
        {
            
            try
        {
            using (con)
            {
                con.Open();

                 string sConsulta = "Update Tarea set Vigente = @Vigente , Rut_Asignado=@Rut_Asignado " +
                      " Where id_tarea = @id_tarea ";
                      

                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;


                
                query.Parameters.Add(new SqlParameter("@Id_Tarea", t.Id_Tarea));
                                    
                query.Parameters.Add(new SqlParameter("@Rut_Asignado", t.Rut_Asignado));

                query.Parameters.Add(new SqlParameter("@Vigente", t.Vigente));

        

                    query.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            throw;
        }


    }
        public void Completar(Tarea t)
        {

            try
            {
                using (con)
                {
                    con.Open();

                    string sConsulta = "Update Tarea set Vigente = 0 , Fecha_Termino= getDate() ,Condicion=@Condicion  " +
                         " Where id_tarea = @id_tarea ";


                    var query = new SqlCommand(sConsulta, con);

                    query.CommandType = CommandType.Text;



                    query.Parameters.Add(new SqlParameter("@Id_Tarea", t.Id_Tarea));
                    query.Parameters.Add(new SqlParameter("@Condicion", t.Condicion));



                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
            public void Eliminar(int idTarea)
        {
            
            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand("tarea_eliminar", con);

                    query.CommandType = CommandType.StoredProcedure;

                    query.Parameters.Add(new SqlParameter("@Id_Tarea", idTarea));

                    query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }


        }


        


    }
    }
