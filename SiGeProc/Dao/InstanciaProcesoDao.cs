﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeProc;
using SiGeProc.Entidades;

namespace SiGeProc.Dao
{
    public interface InstanciaProcesoDao
    {


        int Agregar(InstanciaProceso ip);

        void Actualizar(InstanciaProceso ip);


        void Completar(int id);





    }
}
