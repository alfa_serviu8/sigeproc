﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeProc;
using SiGeProc.Entidades;

namespace SiGeProc.Dao
{
    public interface SimboloDao
    {
        Simbolo PorId(int idSimbolo);

        int Agregar(Simbolo s);

        
        List<Simbolo> PorModelo(int idModelo);

        void Eliminar(int idSimbolo, int idModelo);
    }
}
