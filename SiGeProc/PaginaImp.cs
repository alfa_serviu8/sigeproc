﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiGeProc
{
    public class PaginaImp:IPagina
    {
        int numero;
        int tamacno;


        public PaginaImp() : this(1, 100000)
        {
           
        }

        public PaginaImp(int numero,int tamacno)
        {
            this.numero = numero;
            this.tamacno = tamacno;

        }

        public int Numero()
        {
             return this.numero;

        }

        public int Tamacno()
        {
            return this.tamacno;

        }
    }
}