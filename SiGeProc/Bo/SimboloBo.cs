﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeProc.Dao;
using SiGeProc.Entidades;

namespace SiGeProc.Bo
{
    public interface SimboloBo
    {
        Simbolo PorId(int idSimbolo);
        List<Simbolo> PorModelo(int idModelo);

        Simbolo EventoInicio(int idModelo);

        int Agregar(Simbolo s);

        int AgregarEventoInicio(int idModelo);

        int AgregarTareaUsuario(int idModelo);

        int AgregarCompuertaExclusiva(int idModelo);

        int AgregarEventoFin(int idModelo);

        void Eliminar(int idSimbolo, int idModelo);


    }
}
