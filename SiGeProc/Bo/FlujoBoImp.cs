﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc.Dao;



namespace SiGeProc.Bo
{
    public class FlujoBoImp:FlujoBo
    {
        FlujoDao fDao;

        public FlujoBoImp()
        {
            fDao = new FlujoDaoImp();
                

        }


       public List<Flujo> PorModelo(int idModelo)
        {

            return new FlujoDaoImp().PorModelo(idModelo);

        }


        

        public List<Simbolo> Siguientes(int idSimboloActual, int condicion, int idInstanciaProceso)
        {

            List<Simbolo> tareas= new List<Simbolo>();

            List<Flujo> siguientes = new FlujoDaoImp().Siguientes(idSimboloActual);

            foreach(Flujo f in siguientes)
            {

                if(new SimboloBoImp().PorId(f.Id_Simbolo_Siguiente).EsCompuertaParalela())
                {
                    //tareas.Add(f.Id_Simbolo_Siguiente);
                    if (condicion == f.Condicion)
                    {
                        tareas = Siguientes(f.Id_Simbolo_Siguiente, condicion, idInstanciaProceso);
                    
                    }

                }

                if (new SimboloBoImp().PorId(f.Id_Simbolo_Siguiente).EsTareaUsuario())             {

                    if (condicion == f.Condicion)
                    {
                        tareas.Add(new SimboloBoImp().PorId(f.Id_Simbolo_Siguiente));
                    }
                    //tareas = Siguientes(f.Id_Simbolo_Siguiente, condicion, idModelo);

                }
                if (new SimboloBoImp().PorId(f.Id_Simbolo_Siguiente).EsCompuertaExclusiva())
                {
                    tareas = Siguientes(f.Id_Simbolo_Siguiente, condicion, idInstanciaProceso);

                }
            }



            return tareas;
            
        }



        public List<Flujo> Anteriores(int idSimboloActual)
        {

            return fDao.Anteriores(idSimboloActual);

        }











    }
 }
