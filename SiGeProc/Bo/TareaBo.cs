﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeProc;
using SiGeProc.Entidades;

namespace SiGeProc.Bo
{
    public interface TareaBo
    {
        PaginaTarea Listado(TareaBuscada tb);

        Tarea PorId(int idTarea);



        List<Tarea> Completadas(int idInstanciaProceso);

        List<Tarea> Vigentes(int idInstanciaProceso);


        int Agregar(Tarea t);
        void Actualizar(Tarea t);
        void Completar(Tarea t);

    }
}
