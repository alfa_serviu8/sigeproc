﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeProc;
using SiGeProc.Entidades;

namespace SiGeProc.Dao
{
    public interface FlujoBo
    {
       
        List<Simbolo> Siguientes(int idSimboloActual, int condicion,  int idInstanciaProceso);

    


        List<Flujo> Anteriores(int idSimboloActual);

        List<Flujo> PorModelo(int idModelo);


    }
}
