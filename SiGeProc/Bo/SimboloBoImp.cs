﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc.Dao;



namespace SiGeProc.Bo
{
    public class SimboloBoImp:SimboloBo
    {
        SimboloDao  sDao ;

        public SimboloBoImp()
        {
            sDao = new SimboloDaoImp();
                

        }
        public List<Simbolo> PorModelo(int idModelo)
        {
            return new SimboloDaoImp().PorModelo(idModelo);
             
                   

        }

        public int Agregar(Simbolo s)
        {

            return new SimboloDaoImp().Agregar(s);
        }


        public int AgregarEventoInicio(int idModelo) {

            Simbolo s = new Simbolo(idModelo, new Tipo_Simbolo().EventoInicio(), "Inicio", 0, 0, "");
            return Agregar(s);
        
        }

        public int AgregarTareaUsuario(int idModelo)
        {

            Simbolo s = new Simbolo(idModelo, new Tipo_Simbolo().TareaUsuario(), "Revisar Expediente", 0, 0, "");
            return Agregar(s);

        }

        public int AgregarCompuertaExclusiva(int idModelo) {

            Simbolo s = new Simbolo(idModelo, new Tipo_Simbolo().CompuertaExclusiva(), "Compuerta", 0, 0, "");
            return Agregar(s);


        }

        public int AgregarEventoFin(int idModelo)
        {

            Simbolo s = new Simbolo(idModelo, new Tipo_Simbolo().EventoFin(), "Fin", 0, 0, "");
            return Agregar(s);


        }


        public void Eliminar(int idSimbolo, int idModelo)
        {

            new SimboloDaoImp().Eliminar(idSimbolo,idModelo);

        }

        public Simbolo PorId(int idSimbolo)
        {
        
          
            return new SimboloDaoImp().PorId(idSimbolo);
        }
        public Simbolo EventoInicio(int idModelo)
        {
            List<Simbolo> simbolos = PorModelo(idModelo);
           
          
            int i = 0;
      
            while (i < simbolos.Count && simbolos[i].Tipo_Simbolo != "EventoInicio")
            {
                i++;
            }

           

            return simbolos[i];



        }

    }
 }
