﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeProc;
using SiGeProc.Entidades;

namespace SiGeProc.Bo
{
    public interface ModeloBo
    {
        List<Modelo> PorProceso(int idProceso);

        Modelo Ultimo(int idProceso);
        
      
    }
}
