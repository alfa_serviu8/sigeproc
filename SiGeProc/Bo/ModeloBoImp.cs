﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc.Dao;



namespace SiGeProc.Bo
{
    public class ModeloBoImp:ModeloBo
    {
        ModeloDao mDao; 
        public ModeloBoImp()
        {
            mDao= new ModeloDaoImp();


        }
        public List<Modelo> PorProceso(int idProceso)
        {
            return mDao.PorProceso(idProceso);
        }

        public Modelo Ultimo(int idProceso)
        {
            List<Modelo> modelos = new List<Modelo>();
           
            modelos= mDao.PorProceso(idProceso);
            
            return modelos[modelos.Count - 1];
        }

    }
 }
