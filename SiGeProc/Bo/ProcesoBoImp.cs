﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc.Dao;



namespace SiGeProc.Bo
{
    public class ProcesoBoImp : ProcesoBo
    {
        ProcesoDao pDao;

        public ProcesoBoImp()
        {
            pDao = new ProcesoDaoImp();


        }
        public PaginaProceso Listado(ProcesoBuscado pBuscado)
        {

            return pDao.Listado(pBuscado);


        }


        public Proceso PorSistema(int idSistema)
        {


            return pDao.PorSistema(idSistema);
        }

        public List<int> IniciarProceso(int idProceso, int condicion, out int idInstanciaProceso)
        {

            Modelo ultimo = new ModeloBoImp().Ultimo(idProceso);

            // obtiene el evento de inicio asociado al modelo
            Simbolo si = new SimboloBoImp().EventoInicio(ultimo.Id_Modelo);

            //agrega las instancia de proceso

            idInstanciaProceso = new InstanciaProcesoBoImp().Agregar(new InstanciaProceso(idProceso));

            List<Simbolo> tareas = new List<Simbolo>();

            tareas = new FlujoBoImp().Siguientes(si.Id_Simbolo, condicion, idInstanciaProceso);

            //genera las tareas encontradas
            List<int> listadoIdTareas = new List<int>();

            foreach (Simbolo s in tareas)
            {

                listadoIdTareas.Add(new TareaBoImp().Agregar(new Tarea(s, idInstanciaProceso)));
            }

            return listadoIdTareas;

        }
        public List<int> IniciarProceso(int idProceso, out int idInstanciaProceso)
        {
          
          //obtiene el ultimo modelo asociado al proceso
          
            Modelo ultimo = new ModeloBoImp().Ultimo(idProceso);

            // obtiene el evento de inicio asociado al modelo
            Simbolo si = new SimboloBoImp().EventoInicio(ultimo.Id_Modelo);

                       
            

          //agrega las instancia de proceso

            idInstanciaProceso = new InstanciaProcesoBoImp().Agregar(new InstanciaProceso(idProceso));

            //busca las siguientes simbolos que son tareas


            List<Simbolo> tareas = new List<Simbolo>();

            tareas = new FlujoBoImp().Siguientes(si.Id_Simbolo, -1, idInstanciaProceso);

            //genera las tareas encontradas
            List<int> listadoIdTareas = new List<int>();

            foreach (Simbolo s in tareas)
            {
               
                listadoIdTareas.Add(new TareaBoImp().Agregar(new Tarea(s, idInstanciaProceso)));
            }

            return listadoIdTareas;
        }

       

        public List<int> TareasSiguientes(Tarea tareaActual,int condicion)
        {
                                 
                        

            //busca las siguientes simbolos que son tareas

            List<Simbolo> tareas = new List<Simbolo>();

            tareas =new FlujoBoImp().Siguientes(tareaActual.Id_Tipo_Tarea, condicion, tareaActual.Id_Instancia_Proceso);

            //genera las tareas encontradas

            List<int> listadoIdTareas = new List<int>();

            foreach (Simbolo s in tareas)
            {
                listadoIdTareas.Add(new TareaBoImp().Agregar(new Tarea(s, tareaActual.Id_Instancia_Proceso)));
            }

            // completa la tarea actual

            new TareaBoImp().Completar(tareaActual);

            return listadoIdTareas;
            
                       
        }


    }

    }
 
