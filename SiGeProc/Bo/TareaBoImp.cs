﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;
using SiGeProc.Dao;



namespace SiGeProc.Bo
{
    public class TareaBoImp:TareaBo
    {
        TareaDao tDao = new TareaDaoImp();

        public TareaBoImp()
        {
            

        }
        public PaginaTarea Listado(TareaBuscada tb)
        {
            return tDao.Listado(tb);
           
        }


        public List<Tarea> Completadas(int idInstanciaProceso)
        {
            PaginaTarea pt = Listado(new TareaBuscada(new Tarea(0, idInstanciaProceso), new PaginaImp()));

            return pt.tareas;

        }

        public List<Tarea> Vigentes(int idInstanciaProceso)
        {
            PaginaTarea pt = Listado(new TareaBuscada(new Tarea(1, idInstanciaProceso), new PaginaImp()));

            return pt.tareas;

        }

        public int Agregar(Tarea t)
        {

            return tDao.Agregar(t);
            
        }

        public Tarea PorId(int idTarea)
        {

            return tDao.Listado(new TareaBuscada(new Tarea(idTarea), new PaginaImp())).tareas[0];

        }


    

        public void Actualizar(Tarea t)
        {

            tDao.Actualizar(t);

        }

        public void Completar(Tarea t)
        {

            tDao.Completar(t);

        }



    }
    }
