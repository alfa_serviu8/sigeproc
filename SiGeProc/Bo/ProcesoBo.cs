﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeProc;
using SiGeProc.Entidades;

namespace SiGeProc.Bo
{
    public interface ProcesoBo
    {
        PaginaProceso Listado(ProcesoBuscado pBuscado);
   
        List<int> IniciarProceso(int idProceso,out int idInstanciaProceso);

        List<int> IniciarProceso(int idProceso,int condicion, out int idInstanciaProceso);

        List<int> TareasSiguientes(Tarea tareaActual,int condicion);

        Proceso PorSistema(int idSistema);

    }
}
