﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;

namespace SiGeProc.Entidades
{
    public class PaginaProceso
    {
        public List<Proceso> procesos { set; get; }
        public int totalRegistros { set; get; }

        public PaginaProceso(List<Proceso> procesos,int totalRegistros)
        {

            this.procesos = procesos;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}