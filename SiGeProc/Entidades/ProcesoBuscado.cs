﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc;
using SiGeProc.Entidades;

namespace SiGeProc.Entidades
{
    public class ProcesoBuscado
    {
        public Proceso proceso { set; get; }
        public PaginaImp pagina { set; get; }
                 
        
        public ProcesoBuscado(Proceso p,PaginaImp pag)
        {

            this.proceso = p;
            this.pagina = pag;
                           
        }

        public ProcesoBuscado()
        {

           
        }



    }

}