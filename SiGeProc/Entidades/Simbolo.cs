﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;

namespace SiGeProc.Entidades
{
    public class Simbolo 
    {
        public int Id_Simbolo { set; get; }
        public int Id_Modelo { set; get; }
        public string Tipo_Simbolo { set; get; }
        public string Nombre_Simbolo { set; get; }
        public int Rut_Asignado { set; get; }
        public int Rol_Asignado { set; get; }
        public string Formulario { set; get; }



        public Simbolo(int idModelo,string tipoSimbolo,string nombreSimbolo,int rutAsignado,int rolAsignado,string formulario)
        {

            this.Id_Modelo = idModelo;
            this.Tipo_Simbolo = tipoSimbolo;
            this.Nombre_Simbolo = nombreSimbolo;
            this.Rut_Asignado = rutAsignado;
            this.Rol_Asignado = rolAsignado;
            this.Formulario = formulario;


        }
            public Simbolo(SqlDataReader dr)
        {
           
                this.Id_Simbolo = Convert.ToInt32(dr["Id_Simbolo"]);
                this.Id_Modelo = Convert.ToInt32(dr["Id_Modelo"]);
                this.Nombre_Simbolo = dr["Nombre_Simbolo"].ToString();
                this.Tipo_Simbolo = dr["Tipo_Simbolo"].ToString();
                this.Rut_Asignado = Convert.ToInt32(dr["Rut_Asignado"]);
                this.Rol_Asignado = Convert.ToInt32(dr["Rol_Asignado"]);
                this.Formulario = dr["Formulario"].ToString();



        }


        public Simbolo()
        {


           

        }

        public bool EsEventoInicio()
        {
            return this.Tipo_Simbolo == "EventoInicio";
        }

        public bool EsCompuertaExclusiva()
        {
            return this.Tipo_Simbolo == "CompuertaExclusiva";
        }
     
        public bool EsCompuertaParalela()
        {
            return this.Tipo_Simbolo == "CompuertaParalela";
        }

        public bool EsEventoFin()
        {
            return this.Tipo_Simbolo == "EventoFin";
        }

        public bool EsTareaUsuario()
        {
            return this.Tipo_Simbolo == "TareaUsuario";
        }

       

    }

}