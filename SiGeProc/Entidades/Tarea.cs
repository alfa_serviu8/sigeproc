﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;

namespace SiGeProc.Entidades
{
    public class Tarea
    {
        public int Id_Tarea { set; get; }
        public int Id_Tipo_Tarea { set; get; }
        public int Id_Instancia_Proceso { set; get; }
        public string Nombre_Tarea { set; get; }
        public int Rut_Asignado { set; get; }
        public int Rol_Asignado { set; get; }
        public DateTime Fecha_Inicio { set; get; }
        public DateTime Fecha_Termino { set; get; }
        public int Vigente { set; get; }

        public int Condicion { set; get; }
        public string Observaciones { set; get; }
        public string Formulario {set;get;}
    

        public Tarea(int IdTarea,int idTipoTarea,int rutAsignado, int rolAsignado,int vigente,int idInstanciaProceso)
        {


            this.Id_Tarea = IdTarea;
            this.Id_Tipo_Tarea = idTipoTarea;
            this.Rut_Asignado = rutAsignado;
            this.Rol_Asignado = rolAsignado;
            this.Vigente = vigente;
            this.Id_Instancia_Proceso = idInstanciaProceso;


        }

        public Tarea(int idTarea)
        {


            this.Id_Tarea = idTarea;
            this.Id_Tipo_Tarea = 0;
            this.Rut_Asignado = 0;
            this.Rol_Asignado = 0;
            this.Vigente = -1;
            this.Id_Instancia_Proceso = 0;


        }

        public Tarea(Simbolo s,int idInstanciaProceso)
        {


            this.Id_Tarea = 0;
            this.Id_Tipo_Tarea = s.Id_Simbolo;
            this.Rut_Asignado = s.Rut_Asignado;
            this.Rol_Asignado = s.Rol_Asignado;
            this.Formulario = s.Formulario;
            this.Condicion = -1;
            this.Id_Instancia_Proceso = idInstanciaProceso;


        }

        public Tarea(int vigente,int idInstanciaProceso)
        {


            this.Id_Tarea = 0;
            this.Id_Tipo_Tarea =0;
            this.Rut_Asignado =0;
            this.Rol_Asignado = 0;
            this.Vigente = vigente;
            this.Id_Instancia_Proceso = idInstanciaProceso;


        }




        public Tarea(SqlDataReader dr)
        {
           
                this.Id_Tarea = Convert.ToInt32(dr["Id_Tarea"]);
                this.Id_Tipo_Tarea = Convert.ToInt32(dr["Id_Tipo_Tarea"]);
                this.Id_Instancia_Proceso = Convert.ToInt32(dr["Id_Instancia_Proceso"]);
                this.Nombre_Tarea = dr["Nombre_Tarea"].ToString();
                this.Rut_Asignado = Convert.ToInt32(dr["Rut_Asignado"]);
                this.Rol_Asignado = Convert.ToInt32(dr["Rol_Asignado"]);
                this.Condicion = Convert.ToInt32(dr["Condicion"]);
                this.Fecha_Inicio = Convert.ToDateTime(dr["Fecha_Inicio"]);
                this.Fecha_Termino = Convert.ToDateTime(dr["Fecha_Termino"]);
                this.Vigente = Convert.ToInt32(dr["Vigente"]);
                this.Observaciones = dr["Observaciones"].ToString();
                this.Formulario = dr["Formulario"].ToString();


        }


        public Tarea()
        {


           

        }


        public void Completar()
        {

            this.Vigente = 0;


        }

        public bool EsVigente()
        {
            return this.Vigente == 1;
        }

    }

}