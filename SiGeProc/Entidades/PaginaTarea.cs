﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;

namespace SiGeProc.Entidades
{
    public class PaginaTarea
    {
        public List<Tarea> tareas { set; get; }
        public int totalRegistros { set; get; }

        public PaginaTarea(List<Tarea> tareas,int totalRegistros)
        {

            this.tareas = tareas;
                        
            this.totalRegistros = totalRegistros;
        }
    }
        
}