﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc;
using SiGeProc.Entidades;

namespace SiGeProc.Entidades
{
    public class TareaBuscada
    {
        public Tarea tarea { set; get; }
        public PaginaImp pagina { set; get; }
                 
        
        public TareaBuscada(Tarea t,PaginaImp pag)
        {

            this.tarea = t;
            this.pagina = pag;
                           
        }

        public TareaBuscada()
        {

           
        }



    }

}