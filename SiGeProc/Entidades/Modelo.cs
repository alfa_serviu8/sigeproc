﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;

namespace SiGeProc.Entidades
{
    public class Modelo 
    {
        public int Id_Modelo  { set; get; }
        public int Id_Proceso { set; get; }
        public int Version { set; get; }
   


        public Modelo(int idModelo,int idProceso, int version)
        {


            this.Id_Modelo = idModelo;
            
            this.Id_Proceso = idProceso;
            
            this.Version = version;
           


        }
            public Modelo(SqlDataReader dr)
        {
                this.Id_Modelo = Convert.ToInt32(dr["Id_Modelo"]);
                this.Id_Proceso = Convert.ToInt32(dr["Id_Proceso"]);
                this.Version = Convert.ToInt32(dr["Version"]);


        }


        public Modelo()
        {
            

        }

        public Modelo(int idProceso)
        {

            this.Id_Proceso = idProceso;
            this.Id_Modelo = Constants.ModeloPorDefecto;
            this.Version = Constants.VersionPorDefecto;

        }

        static class Constants
        {
            public const int ModeloPorDefecto = 1;
            public const int VersionPorDefecto = 1;

        }


    }



}