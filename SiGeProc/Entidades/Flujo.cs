﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;

namespace SiGeProc.Entidades
{
    public class Flujo 
    {

        public int Id_Simbolo { set; get; }

        public int Id_Simbolo_Siguiente { set; get; }

        public int Id_Modelo { set; get; }

        public int  Condicion { set; get; }
        
        public Flujo(int idSimboloSiguiente,int idSimbolo,int condicion)
        {

            this.Id_Simbolo = idSimbolo;
            this.Id_Simbolo_Siguiente = idSimboloSiguiente;
            this.Condicion = condicion;
           

        }
            public Flujo(SqlDataReader dr)
        {


            this.Id_Simbolo = Convert.ToInt32(dr["Id_Simbolo"]);
            this.Id_Modelo = Convert.ToInt32(dr["Id_Modelo"]);
            this.Id_Simbolo_Siguiente = Convert.ToInt32(dr["Id_Simbolo_Siguiente"]);
            this.Condicion = Convert.ToInt32(dr["Condicion"]);
            



        }


        public Flujo()
        {


           

        }

       

    }

}