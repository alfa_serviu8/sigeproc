﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiGeProc.Entidades
{
    class Tipo_Simbolo
    {
        string tipoSimbolo { set; get; }

        public string EventoInicio()
        {
            return "EventoInicio";
        }

        public string EventoFin()
        {
            return "EventoFin";
        }
        public string CompuertaExclusiva()
        {
            return "CompuertaExclusiva";
        }

        public string CompuertaParalela()
        {
            return "CompuertaParalela";
        }

        public string TareaUsuario()
        {
            return "TareaUsuario";
        }

        public string EsEventoFin()
        {
            return EventoFin();
        }

        public string EsTareaUsuario()
        {
            return TareaUsuario();
        }


    }
}
