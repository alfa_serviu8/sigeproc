﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;

namespace SiGeProc.Entidades
{
    public class Proceso 
    {
        public int Id_Proceso { set; get; }

        public int Id_Sistema { set; get; }

        public int EsInterno { set; get; }
        public string Codigo_Proceso { set; get; }
        public string Nombre_Proceso { set; get; }
        public int Numero_Resolucion { set; get; }

        public DateTime Fecha_Resolucion { set; get; }
        public int Vigente { set; get; }
   


        public Proceso(int idProceso,string codigoProc, string nombreProceso,int vigente)
        {


            this.Id_Proceso = idProceso;
            this.Codigo_Proceso = codigoProc;
            this.Nombre_Proceso = nombreProceso;
            this.Vigente = vigente;


        }
            public Proceso(SqlDataReader dr)
        {
           
                this.Id_Proceso = Convert.ToInt32(dr["Id_Proceso"]);
                this.Numero_Resolucion = Convert.ToInt32(dr["Numero_Resolucion"]);
                this.Fecha_Resolucion = Convert.ToDateTime(dr["Fecha_Resolucion"]);
                this.Codigo_Proceso = dr["Codigo_Proceso"].ToString();
                this.Nombre_Proceso = dr["Nombre_Proceso"].ToString();
                this.Vigente = Convert.ToInt32(dr["Vigente"]);


        }


        public Proceso()
        {
           

        }
      

    }

}