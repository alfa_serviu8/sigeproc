﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeProc.Entidades;

namespace SiGeProc.Entidades
{
    public class InstanciaProceso 
    {
        public int Id_Instancia_Proceso { set; get; }
        public int Id_Proceso { set; get; }
        public DateTime Fecha_Inicio { set; get; }

        public DateTime Fecha_Termino { set; get; }

        public string Estado { set; get; }
   


        public InstanciaProceso(int idInstanciaProceso,int idProceso, string estado)
        {

            this.Id_Instancia_Proceso = idInstanciaProceso;
            this.Id_Proceso = idProceso;
            this.Estado = estado;
         


        }

        public InstanciaProceso(int idProceso)
        {

            this.Id_Instancia_Proceso = 0;
            this.Id_Proceso = idProceso;
            this.Estado = "";



        }
        public InstanciaProceso(SqlDataReader dr)
        {
            this.Id_Instancia_Proceso = Convert.ToInt32(dr["Id_Instancia_Proceso"]);
            this.Id_Proceso = Convert.ToInt32(dr["Id_Proceso"]);
            this.Fecha_Inicio = Convert.ToDateTime(dr["Fecha_Inicio"]);
            this.Fecha_Termino = Convert.ToDateTime(dr["Fecha_Termino"]);
            this.Estado = dr["Estado"].ToString();
               


        }


        public InstanciaProceso()
        {
           

        }

        public bool EstaActiva()
        {
            return Estado == "Activa";
        
        }

        public bool EstaCompletada()
        {
            return Estado == "Completada";

        }

    }

}